// SECTION - Comparison Query Operators

// $gt/$gte    --> greater than or greater than or equal
/*
Syntax: 

db.collectionName.find({field : {$gt : value}});
db.collectionName.find({field : {$gte : value}});

*/

db.users.find({age : {$gt :50}});
db.users.find({age : {$gte :50}});


// $lt/$lte operator --> less than or less than or equal

/*
Syntax: 

db.collectionName.find({field : {$lt : value}});
db.collectionName.find({field : {$lte : value}});

*/

db.users.find({age : {$lt :50}});
db.users.find({age : {$lte :50}});

// $ne operator --> not equal

/*
Syntax: 

db.collectionName.find({field : {$ne : value}});


*/

db.users.find({age : {$ne :82}});


// $in operator   --> specific match criteria 

/*
Syntax: 

db.collectionName.find({field : {$in : value}});


*/

db.users.find ( {lastName : {$in : ["Hawking", "Doe"]}});
db.users.find ( {courses : {$in : ["HTML", "React"] } } );

// $or operator

// $or operator   -->  either of the given criteria

/*
Syntax: 

db.collectionName.find( { $or [ {fieldA : valueA}, {fieldB : valueB} ] } );


*/

db.users.find( {$or : [{firstName : "Neil"}, {age : 25}]});
db.users.find({$or : [{firstName : "Neil"}, {age : {$gt : 30} ] } );



// $and operator

// $or operator   -->  all of the given criteria

/*
Syntax: 

db.collectionName.find( { $and [ {fieldA : valueA}, {fieldB : valueB} ] } );


*/

db.users.find( {$and : [ {age : {$ne : 82} }, {age : {$ne : 76} } ] } );


// SECTION - Field Projection

// Inclusion  ---> allows us to include/add specific field only when retrieving documnets
// --> 1 to denotes inclusion

/*
Syntax: 

db.collectionName.find({criteria}, {firld: 1})


*/

db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1
}

);

// Inclusion  ---> allows us to exclude/remove specific field only when retrieving documnets
// --> o to denotes exclusion

/*
Syntax: 

db.collectionName.find({criteria}, {firld: 1})


*/

db.users.find(
{
	firstName: "Jane"
},
{
	contact: 0,
	department: 0
}

);


// Suppressing the ID field

db.users.find(
{
	firstName: "Jane"

},
{
	firstName: 1,
	lastName: 1,
	contact: 1,
	_id: 0
}

);

db.users.find(
{
	firstName: "Jane"

},
{
	firstName: 1,
	lastName: 1,
	"contact.phone": 1,
}

);

db.users.find(
{
	firstName: "Jane"

},
{
	"contact.phone": 0,
}

);

// $slice operator - allows us to retrieve only 1 element that matched the search criteria

db.users.find(
{ "nameArr" : 
	{
	namea: "juan"
	}
},
{
	namearr: {$slice :1}
}
);

// SECTION - Evaluation Query Operators

// $regex operator 


// For Case sensetive query
/*
Syntax:

db.users.find({field: $regex: 'pattern', $options: '$optionValue'});
*/

db.users.find({firstName: {$regex: "N"}});



// For Case insensetive query
//  '$i' ---> i - means insensetive

db.users.find({firstName: {$regex: "j", $options: '$i'}});

